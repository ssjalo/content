# Kaikenlaisia hukkaskriptejä tänne odottamaan hyötykäyttöä!
########################################################################



# Datan lataaminen
# luodaan kansio "data"
dir.create("./data")
# ladataan FAOSTAT-tietokannasta 
download.file(url = "http://fenixservices.fao.org/faostat/static/bulkdownloads/Production_Livestock_E_All_Data_(Norm).zip", 
              mode = "wb",
              destfile = "./data/Production_Livestock_E_All_Data_(Norm).zip")
# puretaan zip
unzip(zipfile = "./data/Production_Livestock_E_All_Data_(Norm).zip", exdir = "./data")
# ladataan data R:ään
d <- read.csv("./data/Production_Livestock_E_All_Data_(Norm).csv", stringsAsFactors = FALSE)
# Kuusi ensimmäistä riviä
head(d)
# rivien ja muuttujien määrät
dim(d)

# Suomessa 2010 tuotettujen eläimien määrät
library(dplyr)
library(ggplot2)

# Suomen elukat
d %>% filter(Country == "Finland",
             Year == 2010,
             Unit == "Head") -> dd

dim(dd)

p <- ggplot(data=dd, aes(x=Item, y=Value))
p <- p + geom_bar(stat="identity", position="dodge")
p 

# Pohjoimaiden elukat
d %>% filter(Country %in% c("Finland","Sweden","Norway","Denmark", "Iceland"),
             Year == 2010,
             Unit == "Head") %>% 
  select(Item,Value,Country) -> dd

dim(dd)

p <- ggplot(data=dd, aes(x=Country, y=Value))
p <- p + geom_bar(stat="identity", position="dodge")
p <- p + facet_wrap(~Item, scales = "free_x")
p 

